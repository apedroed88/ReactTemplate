import React, { Component } from 'react';
import {BrowserRouter as Router , Route} from 'react-router-dom'
import Home from './scenes/home/index'
import Sobre from './scenes/sobre/index'
import NavBar from './components/Navbar'

class Routes extends Component {
    render() {
        return (
            <Router>
                <div>
                    <NavBar />                    
                    <Route exact path='/home' component={Home} />
                    <Route path='/sobre' component={Sobre} />
                </div>
            </Router>
        );
    }
}

export default Routes;